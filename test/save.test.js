import pjsql from "../src/pjsql"

beforeEach(() => {
    pjsql.useFile("test.embeded.json")
    pjsql.dropAllDatabase()
    pjsql.useDB("test_db")
});

test('Normal Use', () => {
    const simple = {a:1, b:2}
    expect(pjsql.save("users", simple)).toBe(true);
    expect(pjsql.find("users", (user) => user.a == 1 && user.b == 2)[0]).toBeDefined()
});

test("Multiple Find", () => {
    const simple1 = {a:1, b:1}
    const simple2 = {a:1, b:2}
    const simple3 = {a:1, b:3}
    expect(pjsql.save("users", simple1)).toBe(true);
    expect(pjsql.save("users", simple2)).toBe(true);
    expect(pjsql.save("users", simple3)).toBe(true);
    expect(pjsql.find("users", (user) => user.a == 1).length).toBe(3)
})

test("Find First", () => {
    const simple1 = {a:1, b:1}
    const simple2 = {a:1, b:2}
    const simple3 = {a:1, b:3}
    expect(pjsql.save("users", simple1)).toBe(true);
    expect(pjsql.save("users", simple2)).toBe(true);
    expect(pjsql.save("users", simple3)).toBe(true);
    //expect(pjsql.findFirst("users", (user) => user.a == 1).b).toBe(1)
})

