import pjsql from "../src/pjsql"

beforeEach(() => {
    pjsql.useFile("test.embeded.json")
    pjsql.dropAllDatabase()
    pjsql.useDB("test_db")
});

test('Save and Find by field', () => {
    pjsql.save("users", {a:1})
    expect(pjsql.find("users", (item) => item.a == 1).length).toBe(1) ;
    pjsql.save("users", {a:1})
    expect(pjsql.find("users", (item) => item.a == 1).length).toBe(2) ;
});

test('ChangeDB', () => {
    expect(pjsql.context.databaseName).not.toBe("maDB")
    pjsql.save("users", {a:1})
    expect(pjsql.find("users", (item) => item.a == 1).length).toBe(1) ;
    pjsql.useDB("maDB");
    expect(pjsql.context.databaseName).toBe("maDB")
    pjsql.save("users", {a:1})
    expect(pjsql.find("users", (item) => item.a == 1).length).toBe(1) ;
})

test('Create And Delete TableInNewFile', () => {
    pjsql.useFile("alorsOnDanse.json");
    expect(pjsql.context.databaseName).toBe("");
    expect(pjsql.context.fileName).toBe("alorsOnDanse.json");

    pjsql.useDB("db50");
    expect(pjsql.context.databaseName).toBe("db50");
    pjsql.createTable("uneTable");
    pjsql.dropTable("uneTable")

    pjsql.dropDatabase();
    expect(pjsql.context.databaseName).toBe("")
    pjsql.dropFile("alorsOnDanse.json")
    expect(pjsql.context.fileName).toBe("")
})