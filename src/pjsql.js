import { saveObject, createTable, createDatabase } from "./interactions/PurpleInsert";
import PurpleContext from "./interactions/PurpleContext";
import { findObjects, findOneObject } from "./interactions/PurpleSelect";
import { deleteItem, deleteXItem, dropDatabase, dropAllDatabase, dropFile, dropTable } from "./interactions/PurpleDelete";

export class PurpleJSQL {
    constructor(){
        this.context = new PurpleContext("db1", "embeded.json");
    }

        //////////
        // Uses //
        //////////

    useDB(databaseName, fileName){
        if(fileName) this.useFile(fileName)
        if(!fileName) fileName=this.context.fileName;
        if(!databaseName) return;
        this.context.useDB(databaseName, fileName)
    }

    useFile(fileName){
        if(!fileName) return;
        this.context.useFile(fileName)
    }

        //////////
        // Save //
        //////////

    save(tableName, obj, databaseName, fileName){
        if(!obj) return false;
        if(!tableName) return false;
        if(!databaseName) databaseName = this.context.databaseName;
        if(!fileName) fileName = this.context.fileName;
        return saveObject(obj, tableName, databaseName, fileName)
    }
    
        //////////
        // Find //
        //////////

    find(tableName, criterias, databaseName, fileName){
        if(!fileName) fileName=this.context.fileName;
        if(!databaseName) databaseName=this.context.databaseName;
        if(!tableName) return false;
        return findObjects(tableName, criterias, databaseName, fileName);
    }

    findFirst(tableName, criterias, databaseName, fileName){
        if(!fileName) fileName=this.context.fileName;
        if(!databaseName) databaseName=this.context.databaseName;
        if(!tableName) return false;
        return findOneObject(tableName, criterias, databaseName, fileName);
    }

    findAll(tableName, databaseName, fileName){
        this.find(tableName, undefined, databaseName, fileName);
    }

    //////////////
    //  Delete  //
    //////////////

    delete(tableName, criterias, databaseName, fileName){
        if(!tableName) tableName = "";
        if(!criterias) return;
        if(!databaseName) databaseName = this.context.databaseName;
        if(!fileName) fileName = this.context.fileName;        
        deleteItem(tableName, criterias, databaseName, fileName);
    }

    deleteOne(tableName, criterias, databaseName, fileName){
        this.deleteX(1, tableName, criterias, databaseName, fileName)
    }

    deleteTwo(tableName, criterias, databaseName, fileName){
        this.deleteX(2, tableName, criterias, databaseName, fileName)
    }

    deleteX(x, tableName, criterias, databaseName, fileName){
        if(!tableName) tableName = "";
        if(!criterias) return;
        if(!databaseName) databaseName = this.context.databaseName;
        if(!fileName) fileName = this.context.fileName;        
        deleteXItem(x, tableName, criterias, databaseName, fileName);
    }
    
    //////////////
    //  Struct  //
    //////////////

    createTable(tableName, databaseName, fileName){
        if(!tableName) return;
        if(!databaseName) databaseName = this.context.databaseName;
        if(!fileName) fileName=this.context.fileName;
        createTable(tableName, databaseName, fileName)
    }

    createDatabase(databaseName, fileName){
        if(!databaseName) return;
        if(!fileName) fileName=this.context.fileName;
        createDatabase(databaseName, fileName)
    }

    dropTable(tableName, databaseName, fileName){
        if(!tableName) return;
        if(!databaseName) databaseName = this.context.databaseName;
        if(!fileName) fileName = this.context.fileName;
        dropTable(tableName, databaseName, fileName)
    }

    dropDatabase(databaseName, fileName){
        if(!databaseName) databaseName = this.context.databaseName;
        if(!fileName) fileName = this.context.fileName;
        dropDatabase(databaseName, fileName)
        if(this.context.databaseName == databaseName) this.context.databaseName = "";
    }

    dropAllDatabase(fileName){
        if(!fileName) fileName = this.context.fileName;
        dropAllDatabase(fileName)
        this.context.databaseName = "";
    }

    dropFile(fileName){
        if(!fileName) fileName = this.context.fileName;
        dropFile(fileName)
        if(this.context.fileName == fileName){
            this.context.databaseName = "";
            this.context.fileName = "";
        }
    }
}

export default new PurpleJSQL();