import { loadFile } from "../io/PurpleIO";

export function findObjects(tableName, criterias, databaseName, fileName){
    if(!criterias) criterias = (e) => true;
    const json = loadFile(fileName)
    const database = json.databases.find(e => e.name == databaseName);
    if(!database) return false;
    const datatable = database.tables.find(e => e.name == tableName);
    if(!datatable) return false;
    return datatable.values.filter(criterias);
}

export function findOneObject(tableName, criterias, databaseName, fileName){
    const items = findObjects(tableName, criterias, databaseName, fileName)
    if(items.length <= 0) return undefined;
    return items[0];
}