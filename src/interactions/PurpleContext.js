import { loadFile, saveFile } from "../io/PurpleIO";
import { getBlankDatabase, getBlankDataFile } from "./Templates";

export default class PurpleContext{
    constructor(databaseName, fileName){
        this.databaseName = databaseName;
        this.fileName = fileName;
    }

    useDB(databaseName, fileName){
        if(!databaseName) return;
        if(!fileName) fileName=this.fileName;
        const json = loadFile(fileName)
        const database = json.databases.find(e => e.name == databaseName);
        if(!database) json.databases.push(getBlankDatabase(databaseName));
        saveFile(fileName, json);
        this.databaseName = databaseName;
    }

    useFile(fileName){
        if(!fileName) return;
        saveFile(fileName, getBlankDataFile())
        this.fileName = fileName
        this.databaseName = "";
    }
}