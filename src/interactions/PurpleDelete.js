import { loadFile, saveFile, deleteFile} from "../io/PurpleIO";

export function dropFile(fileName){
    return deleteFile(fileName);
}

export function dropDatabase(databaseName, fileName){
    const json = loadFile(fileName)
    json.databases = json.databases.filter(e => e.name != databaseName);
    saveFile(fileName, json);
    return true;
}

export function dropAllDatabase(fileName){
    const json = loadFile(fileName)
    json.databases = []
    saveFile(fileName, json);
    return true;
}

export function dropTable(tableName, databaseName, fileName){
    const json = loadFile(fileName)
    const database = json.databases.find(e => e.name == databaseName);
    if(!database) return;
    if(database.tables.find(e => e.name == tableName)) database.tables.splice(database.tables.indexOf(database.tables.find(e => e.name == tableName)),1);
    saveFile(fileName, json);
    return true;
}

export function deleteItem(tableName, criterias, databaseName, fileName){
    const json = loadFile(fileName)
    const database = json.databases.find(e => e.name == databaseName);
    if(!database) return false;
    const datatable = database.tables.find(e => e.name == tableName)
    if(!datatable) return false;
    while(datatable.values.find(criterias)) datatable.values.splice(datatable.values.indexOf(datatable.values.find(criterias)))
    saveFile(fileName, json)
    return true;
}

export function deleteXItem(x, tableName, criterias, databaseName, fileName){
    const json = loadFile(fileName)
    const database = json.databases.find(e => e.name == databaseName);
    if(!database) return false;
    const datatable = database.tables.find(e => e.name == tableName)
    if(!datatable) return false;
    let count = 0;
    while(datatable.values.find(criterias) && count > x) {
        count ++;
        datatable.values.splice(datatable.values.indexOf(datatable.values.find(criterias)))
    }
    saveFile(fileName, json)
    return true;
}