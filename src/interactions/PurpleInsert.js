import { loadFile, saveFile } from "../io/PurpleIO";
import { getBlankDatabase, getBlankTable } from "./Templates";

export function saveObject(obj, table, databaseName, file){
    const json = loadFile(file)
    if(!json.databases.find(e => e.name == databaseName)) json.databases.push(getBlankDatabase(databaseName));
    const database = json.databases.find(e => e.name == databaseName);

    const datatable = database.tables.find(e => e.name == table);
    if(!datatable) database.tables.push({name:table, values:[obj]});
    else datatable.values.push(obj);
    saveFile(file, json);
    return true;
}


export function createDatabase(databaseName, file){
    if(!databaseName) return;
    if(!file) file=this.file;
    const json = loadFile(file)
    const database = json.databases.find(e => e.name == databaseName);
    if(database) return;
    json.databases.push(getBlankDatabase(databaseName));
    saveFile(file, json);
}


export function createTable(tableName, databaseName, file){
    const json = loadFile(file)
    let database = json.databases.find(e => e.name == databaseName);
    if(!database) json.databases.push(getBlankDatabase(databaseName));
    database = json.databases.find(e => e.name == databaseName);
    const datatable = database.tables.find(e => e.name == tableName);
    if(datatable) return;
    database.tables.push(getBlankTable(tableName));
    saveFile(file, json);
}