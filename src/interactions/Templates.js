export function getBlankDataFile(){
    return {databases:[{
        name:"db1",tables:[]
    }]}
}

export function getBlankDatabase(databaseName){
    return {name:databaseName,tables:[]};
}

    
export function getBlankTable(tableName){
    return {name:tableName,values:[]};
}