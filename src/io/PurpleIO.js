import fs from "fs";

export default class PurpleIO{
    constructor(){}
}

export function loadFile(path){
    return JSON.parse(fs.readFileSync(path, 'utf-8'));
}

export function saveFile(path, json){
    return fs.writeFileSync(path,JSON.stringify(json, null, 4));
}

export function deleteFile(path){
    return fs.unlinkSync(path)
}