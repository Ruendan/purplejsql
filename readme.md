# PurpleJSQL

PurpleJSQL - Purple Javascript Search Query Language - is made to create quick and easy database access using a JSON file.
It's a kind of NoSQL database managing system made for JS, made to play with really quick or standalone projects.

## The Concept 

The concept is simple :

```js
pjsql require("pjsql");
const user = {name:"Purple", age:18, email:"an@email"};
pjsql.save("user", user);

pjsql.find("user", (user) => user.name == "Purple")
```

And here you are, you succesfully have a user saved in database.


Every informations are stored in a JSON file following a simple architecture, useful to make quick projects in JS.
The goal is to basically retrieve and save things in "database" without a ton of configuration or connection as it's made to have its built-in database. However, you can easily configure it up to fit your requirements !

As it's basically a database stored in a JSON file, if you have any information to check or if you want to modify this very specific line, instead of spending time learning SQL - which you should - you could just wander in the "embeded.json" file, which is... The database and do your things.

It also get rid of the time thinking about how to make your database, as you basically save JSON Objects, without constraint checking. You could have a "User" table with random things inside, as long it is json. It is just meant to be somehow structured but without a lot of thinking. Or just quick project that wouldn't need an entire Database Setup.


## Features

## Use
PurpleJSQL is made to roughly split your bases. Because even if it's made to be quick, it should not let an app accès eachother's data. So you can obviously : Change Database, and Change Datafile.

Quick Explanation :
 - Datafile : A JSON File witch contains Databases
 - Database : A JSON Object witch contains Tables

The Datafile separation is mandatory, as long you don't use the same name for different database you shouldn't have any problem ! These two elements represent the **Context** of your database access. By default, each operation (find, save, delete...) is made on the default database which you can change using the next commands. However, each of these operation can be used with 2 mandatory params : databaseName and fileName, which is the database and datafile you'd like to use for this specific operation.


### Change Datafile

> Let you change datafile. Usualy use it once at the initialisation of your app. It's the only way to change the default datafile.

```js
pjsql.useFile("newDefaultDatafile")
```

### Change Database

> Let you change database. Usualy use it once at the initialisation of your app. It's the only way to change the default database.

```js
// The database is looked in the default datafile if it isn't specified
pjsql.useDB("newDefaultDb")
pjsql.useDB("newDefaultDb", "newDefaultDatafile")
```

## Search
PurpleJSQL let you find users using the same syntax as array filter. Meaning that you don't have to worry about SQL Syntax, you'll just have to write your JSQL Query like you'd find something through an Array (Well, you actually do) 

### Search from Table without Criterias

> Retrieve everything comming from a single table.

```js
const allUsers = pjsql.find("users")
const allUsers2 = pjsql.findAll("users")
```

### Search from Table with Criterias

> Retrieve everything comming from a single table matching JS Array Criterias

```js
const allUsersNamedRuendan = pjsql.find("users", (user) => user.name == "Ruendan")
const allUsersWhoseNameStartsWithR = pjsql.find("users", (user) => user.name.startsWith("R"))
const allUsersWhoHasNoFriends = pjsql.find("users", (user) => user.friendlist.length > 1)
```

### Search First from Table with Criterias

> Retrieve the first result comming from a single table matching JS Array Criterias

```js
const firstUserNamedRuendan = pjsql.findFirst("users", (user) => user.name == "Ruendan")
const firstUserWhoseNameStartsWithR = pjsql.findFirst("users", (user) => user.name.startsWith("R"))
const firstUserWhoHasNoFriends = pjsql.findFirst("users", (user) => user.friendlist.length > 1)

// Search from another database / datafile
const firstUserNamedRuendan = pjsql.findFirst("users", (user) => user.name == "Ruendan", "db2", "another.datafile.json")
```

## Save
The way to save things in database is really simple. That's the whole point of this database management. You give a tableName and the item, and the item is saved in base. That's all. If the table to not exist, it's created. There's no dupplication managment (yet?), no constraint checking, no primary key (yet?). The goal is to make it quick to have something functionnal. Maybe will it be possible in the future but it's not the goal yet.

### Saving Classic

> Saving an Item giving its table.

```js
// no pjsql.createTable("table") needed !
pjsql.save("users", {name:"Ruendan", age:18})
pjsql.save("users", {name:"Ruendan", somethingelse:"Potato"})

// Do not change de default db/datafile
pjsql.save("users", {name:"Ruendan", age:18}, "anotherDB", "anotherDataFileAgain")
```

## Delete
Deleting things is rather hard. At the moment, there's no primary key. No unique fields (all fields being mandatory). It makes things faster but update and delete are rather... Tricky.

Delete Queries are the same as "DELETE .. WHERE". The modifyings are made on ALL data at the same time so you'll have to have really precise criterias.

However, i'm planning on making primary id fields on every objects, letting queries like "find/delete by ID" possible.

There's also a little cheating to remove a given amount of data.

### Delete By Criterias

> Delete items from table with Criterias as JS Filters

```js
// Delete ALL users whose name is Ruendan
pjsql.delete("users", (user) => user.name == "Ruendan")

// Delete ONE "random" users whose name is Ruendan
pjsql.deleteOne("users", (user) => user.name == "Ruendan")

// Delete TWO users whose name is Ruendan
pjsql.deleteTwo("users", (user) => user.name == "Ruendan")

// Delete the X first users whose name is Ruendan
pjsql.deleteX(7, "users", (user) => user.name == "Ruendan")

// Again, specifying DBase and DFile
pjsql.delete("users", (user) => user.name == "Ruendan", "db", "df")
```

## Structure
Even if you don't have to make it explicitely, you can create your database structure explicitely. It could help you making things more clear.

### Create DB/Table

> Creating table and database. It do NOT change the default DB / DataFile.

```js
pjsql.createDatabase("app1DB");
pjsql.createDatabase("app1DB", "anotherSeparateFilename");

pjsql.createTable("commands");
pjsql.createTable("commands", "dbOnSameFile");
pjsql.createTable("commands", "db3", "sepFilename");
```

### Drop DB/Table/AllDB

> Dropping table, database(s) and files. It completely sanityze the data. Use these operations carefully.

```js
pjsql.dropTable("users");
//Other datafile/db...

pjsql.dropDatabase("database");
//,"andDataFile");

pjsql.dropAllDatabase();
//,"andDataFile");

pjsql.dropFile("fileName");
//,"andDataFile");
```

## Author
Quentin "Ruendan" DUBOIS